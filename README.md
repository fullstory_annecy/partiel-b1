## Partiel B1 Développement Front 2H

**!! SANS TOUCHER AU HTML/CSS !!**  
uniquement en javascript avec le fichier main.js

Téléchargez les fichiers grâce au bouton "download/telecharger" dans la barre de gauche
ou rendez-vous sur cette page :  
https://bitbucket.org/fullstory_annecy/partiel-b1/downloads/

Vous devrez m'envoyer un zip de vos fichiers à cette adresse : bertrand@fullstory.fr  
**Votre zip devra respecter la structure suivante "nom-prenom.zip"**


Critères de notation :

1. le code répond à la question
2. qualité du code
3. présence d'erreurs dans la console


---

## Excercice 1

Validez ce formulaire en respectant les points suivants :

1. les champs sont obligatoires
2. l'adresse email doit contenir un '@'
3. le mot de passe doit contenir un minimum de 5 caractères
4. les champs non valides ou manquants seront entourés en rouge et un message d'erreur s'affichera en dessous

![exercice 1 demo](/img/exercice-1.gif)

---

## Excercice 2

A chaque clic la position du cube doit alterner entre "top:0; left:0;" et "right:0; bottom:0"

![exercice 2 demo](/img/exercice-2.gif)

---

## Excercice 3

Au clic sur "nouveau" un produit est ajouté à la liste.

1. Respectez le numero des produits, 1, 2, 3, 4, etc.
2. BONUS ajoutez la possibilité de supprimer un élément

![exercice 3 demo](/img/exercice-3.gif)